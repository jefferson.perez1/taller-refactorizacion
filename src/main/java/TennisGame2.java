
import java.util.HashMap;
import java.util.Map;

public class TennisGame2 implements TennisGame {
    private int player1Points;
    private int player2Points;



    public int getPlayer1Points() {
        return player1Points;
    }

    public void setPlayer1Points(int player1Points) {
        this.player1Points = player1Points;
    }

    public int getPlayer2Points() {
        return player2Points;
    }

    public void setPlayer2Points(int player2Points) {
        this.player2Points = player2Points;
    }

    public String getScore() {
        String score = "";

        if (drawOrDeuce()) {
            score = drawScoreToShow();
        } else if (scorePlayersBeyondForty()) {
            score = getAdvantage();
        } else {
            score = showScoresForPlayers();
        }

        return score;
    }

    private boolean drawOrDeuce() {
        return (getPlayer1Points() == getPlayer2Points());
    }

    private String drawScoreToShow() {
        String[] scores = {"Love-All", "Fifteen-All", "Thirty-All"};
        return getDrawScore(scores);
    }

    private String getDrawScore(String[] scores) {
        return (scoreDrawPlayers() < 3) ? scores[scoreDrawPlayers()] : "Deuce";
    }

    private int scoreDrawPlayers() {
        return this.getPlayer1Points();
    }

    private boolean scorePlayersBeyondForty() {
        return getPlayer1Points() >= 4 || getPlayer2Points() >= 4;
    }

    private String getAdvantage() {
        final String WIN_PLAYER_1 = "Win for player1";
        Map<Integer, String> scoresMap = new HashMap<>();


        scoresMap.put(1, "Advantage player1");
        scoresMap.put(-1, "Advantage player2");
        scoresMap.put(2, WIN_PLAYER_1);
        scoresMap.put(3, WIN_PLAYER_1);
        scoresMap.put(4, WIN_PLAYER_1);

        return scoresMap.getOrDefault(getDifferencePoints(), "Win for player2");

    }

    private int getDifferencePoints() {
        return getPlayer1Points() - getPlayer2Points();
    }

    private String showScoresForPlayers() {
        return obtainScore(getPlayer1Points()) + "-" + obtainScore(getPlayer2Points());
    }

    private String obtainScore(int playerPoints) {
        String[] scores = {"Love", "Fifteen", "Thirty", "Forty"};
        return scores[playerPoints];
    }




    private void increaseP1Score() {
        this.setPlayer1Points(this.getPlayer1Points() + 1);
    }


    private void increaseP2Score() {
        this.setPlayer2Points(this.getPlayer2Points() + 1);
    }

    public void wonPoint(String player) {
        if (player.equalsIgnoreCase("player1")) {
            increaseP1Score();
        } else {
            increaseP2Score();
        }
    }
}
