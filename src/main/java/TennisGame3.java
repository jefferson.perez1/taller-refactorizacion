
public class TennisGame3 implements TennisGame {


    private int player1Points;
    private int player2Points;
    private String player1Name;
    private String player2Name;

    public TennisGame3(String player1Name, String player2Name) {
        this.player1Name = player1Name;
        this.player2Name = player2Name;
    }

    public String getScore() {
        final String DEUCE = "Deuce";
        String playerName;
        String score;
        if (scorePlayersLessThanForty() && gameNotInDeuce()) {
            score = scoreInGame();
        }
        else if (isDraw()){
            score = DEUCE;
        }
        //
        else{
            playerName = getPlayerNameWithMorePoints();
            score = isAdvantage() ? showPlayerInAdvantage(playerName) : showWinnerPlayer(playerName);
        }
        return score;

    }

    private boolean scorePlayersLessThanForty() {
        return player1Points < 4 && player2Points < 4;
    }

    private boolean gameNotInDeuce() {
        return player1Points + player2Points != 6;
    }

    private String scoreInGame() {
        String[] scores = new String[]{"Love", "Fifteen", "Thirty", "Forty"};
        return isDraw() ? getDrawScoreBeforeDeuce(scores[getDrawPlayersPoints()]) : getInGameScoreNotDraw(scores);
    }

    private boolean isDraw() {
        return getPlayer1Points() == getPlayer2Points();
    }

    private String getDrawScoreBeforeDeuce(String score) {
        return score + "-All";
    }

    private int getDrawPlayersPoints (){
        return this.getPlayer1Points();
    }

    private String getInGameScoreNotDraw(String[] scores) {
        return scores[getPlayer1Points()] + "-" + scores[getPlayer2Points()];
    }

    private String getPlayerNameWithMorePoints() {
        return hasPlayer1MorePoints() ? getPlayer1Name() : getPlayer2Name();
    }

    private boolean hasPlayer1MorePoints() {
        return getPlayer1Points() > getPlayer2Points();
    }

    private boolean isAdvantage() {
        return getAbsoluteDifferencePoints() == 1;
    }

    private int getAbsoluteDifferencePoints() {
        return Math.abs(getDifferencePoints());
    }

    private int getDifferencePoints() {
        return getPlayer1Points() - getPlayer2Points();
    }

    private String showPlayerInAdvantage(String savePlayerName) {
        return "Advantage " + savePlayerName;
    }

    private String showWinnerPlayer(String savePlayerName) {
        return "Win for " + savePlayerName;
    }

    public void wonPoint(String playerName) {
        if (playerName.equalsIgnoreCase(getPlayer1Name())){
            this.player1Points += 1;
        }
        else{
            this.player2Points += 1;
        }
    }

    public int getPlayer1Points() {
        return player1Points;
    }

    public int getPlayer2Points() {
        return player2Points;
    }

    public String getPlayer1Name() {
        return player1Name;
    }

    public String getPlayer2Name() {
        return player2Name;
    }
}
